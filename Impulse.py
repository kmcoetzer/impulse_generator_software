# -*- coding: utf-8 -*-
"""
Created on Fri Aug 11 11:04:17 2017

@author: 17511771
"""

from threading import Thread

#Tkinter Stuff
import time
import serial
import threading
import sys
from time import sleep


#MQtt Stuff
import json
import datetime
import sys
from serial import tools
import serial.tools.list_ports

if sys.version_info[0]==2:
        from Tkinter import *
        from Tkinter import messagebox
else:
        from tkinter import *
        from tkinter import messagebox

#CSV stuff
import csv
        
labelBoldFont = "Arial 16 bold"

root = Tk()
root.title("Impulse Generator")
frame1 = Frame(root)


port = StringVar()
port.set("3")
baud_rate = StringVar()
baud_rate.set("115200")
serialPort = serial.Serial()
impulsesRequested = StringVar()
impulsesRequested.set("1")
btnStart1 = StringVar()
btnStart1.set("Start")

btnCon1 = StringVar()
btnCon1.set("Connect")


date = ""

displayVoltage = StringVar()
displayVoltage.set("N/A")
chargeVoltage = StringVar()
chargeVoltage.set("50")
R1 = StringVar()
R1.set("977200")
R2 = StringVar()
R2.set("14753")

displayCount = StringVar()
displayCount.set("0")


chargeStatus = StringVar()
chargeStatus.set("-1")

dischargeStatus = StringVar()
dischargeStatus.set("-1")

enableStatus = StringVar()
enableStatus.set("-1")

setVariablesString = StringVar()
setVariablesString.set("Set Variables")

levelStreamStatus = StringVar()
levelStreamStatus.set("-1")

minimumRechargeVoltage = StringVar()
minimumRechargeVoltage.set("2")

def portScan():
    port.set("Not Found!")
    available = list(tools.list_ports.comports())
    size = len(available)
    i = 0
    j = 0
    while i < size:
        if(available[i][1].startswith("Arduino")):
            s = available[i][0]
            port.set(s.replace("COM",""))
            j+=1
        i+=1

def Connect():
    global serialPort
    global baud_rate
    global port
    global btnCon1
    global writeFile
    global date
    global output
    
    if(port.get()==""):
       portScan()
     
    if(btnCon1.get()=="Connect"):
        try:
            serialPort.baudrate = int(baud_rate.get())
            serialPort.port = 'COM'+port.get()
            
        except Exception:
            print(Exception.with_traceback)
            
            
        try:
            serialPort.open()
            print("serialPort opened")
            btnCon1.set("Disconnect")
            #print("Connection opened")
            """
            thread = threading.Thread(target=readFunction, args=(serialPort,))
            thread.start()
            """
            """
            date = datetime.datetime.now().strftime( "%d-%m-%Y %H.%M.%S" )
            name = 'Test '+str(date)+'.csv'
            with open(name, 'w') as csvfile:
                print("Successfully opened new file")
                
            fields = ["Year", "Month", "Day", "Hour", "Minute", "Second", "Reading"]
            with open (r'Test '+str(date)+'.csv', 'a',newline='') as f:
                writer = csv.writer(f)
                writer.writerow(fields)
            """
            Port1.config(state = DISABLED)
            Baud1.config(state = DISABLED)
            btnDetect1.config(state = DISABLED)
            btnSet.config(state = NORMAL)
            if(setVariablesString.get() == "Edit Variables"):
                btnStart.config(state = NORMAL)
            
            
        except serial.SerialException:
            messagebox.showerror("Connection Error","Cannot Connect!\n\nPlease check that the device is plugged in\nand that the correct COM port is selected.")
            
        except BaseException as e:
            print('{!r}'.format(e))
    


    
    else:
        while(serialPort.isOpen()):
            try:
                serialPort.close()
                displayVal.set("N/A")
                print("Connection closed")
            except Exception:
                print("Cannot close connection!")

        if(serialPort.isOpen() == False):
            btnCon1.set("Connect")
            print("Connection closed")
            Port1.config(state = NORMAL)
            Baud1.config(state = NORMAL)
            btnDetect1.config(state = NORMAL)
            btnSet.config(state = DISABLED)
            btnStart.config(state = DISABLED)
            if(btnStart1.get() == "Start"):
                btnStart1.set("Stop")
            
def startImpulses(ser):
    """
    if(btnCon1.get() == "Disconnect"):    
        try:
            if(ser.isOpen):
                if(btnStart1.get()=="Start"):
                    btnStart1.set("Stop")
                    ##ser.write("C1".encode())
                elif(btnStart1.get()=="Stop"):
                    btnStart1.set("Start")
                    ##ser.write("C0".encode())
        except Exception as e:
            print(e)
    
    if(btnCon1.get() == "Connect"):
        messagebox.showerror("Start Error","Please open a connection before clicking start.")
    """
    
    if(btnStart1.get() == "Start"):
        btnStart1.set("Stop")
        btnSet.config(state = DISABLED)
        executeImpulses(ser)
        
    elif(btnStart1.get() == "Stop"):
        btnStart1.set("Start")
        btnSet.config(state = NORMAL)


    
def readFunction(ser):
    global displayVoltage
    global date
    reading = ""
    if(btnCon1.get() == "Disconnect"):
        try:
            if(ser.isOpen() and ser.in_waiting > 0):
                reading = ser.readline()
                reading = str(reading)[2:]  ## throwing away first two characters
                reading = reading[:-5]      ## throwing away last 6 characters
                ## if received data relates to discharging
                ## print("Received: "+reading)
                if(reading[0:1] == "D"):
                    if(reading[1:2] == "0"):
                        dischargeStatus.set("0")
                        ##print("Discharge stopped")
                    elif(reading[1:2] == "1"):
                        dischargeStatus.set("1")
                        ##print("Discharge started")
                        
                ## if received data relates to charging        
                elif(reading[0:1] == "C"):
                    if(reading[1:2] == "0"):
                        chargeStatus.set("0")
                        ##print("Charge stopped")
                    elif(reading[1:2] == "1"):
                        chargeStatus.set("1")
                        ##print("Charge started")
                        
                ## if received data relates to enable      
                elif(reading[0:1] == "E"):
                    if(reading[1:2] == "0"):
                        enableStatus.set("0")
                        ##print("Enable low")
                    elif(reading[1:2] == "1"):
                        enableStatus.set("1")
                        ##print("Enable high")
                  
                ## if received data relates to charge level  
                elif(reading[0:1] == "L"):
                    if(reading[1:2] == "1"):
                        levelStreamStatus.set("1")
                    elif(reading[1:2] == "0"):
                        levelStreamStatus.set("0") 
                    elif(reading[1:2] == "C"):
                        voltage = float(reading[2:])
                        voltage = voltage*(5/1023)*((float(R1.get())+float(R2.get()))/float(R2.get()))
                        displayVoltage.set(str(round(voltage,1))+" V")
                root.update()
                        
                        
        
                    
        except Exception as e:
            print(e)
            
def setVariables():
    if(setVariablesString.get() == "Set Variables"):
        ##print("Variables set!")
        setVariablesString.set("Edit Variables")
        chargeVoltageNum.config(state = DISABLED)
        ImpulseNumber.config(state = DISABLED)
        btnStart.config(state = NORMAL)
        
    elif(setVariablesString.get() == "Edit Variables"):
        ##print("Variables now editable.")
        setVariablesString.set("Set Variables")
        chargeVoltageNum.config(state = NORMAL)
        ImpulseNumber.config(state = NORMAL)
        btnStart.config(state = DISABLED)
    


def executeImpulses(ser):
    while(int(displayCount.get()) < float(impulsesRequested.get())):
        ## ensure enable is set to zero
        ser.write("E0".encode())
        while(enableStatus.get() != "0"):
            readFunction(ser)
    
        ## ensure discharge is set to zero
        ser.write("D0".encode())
        while(dischargeStatus.get() != "0"):
            readFunction(ser)
    
        ## start charging
        ser.write("C1".encode())
        while(chargeStatus.get() != "1"):
            readFunction(ser)
    
        ## waiting for capacitor bank to charge
        while(displayVoltage.get() == "N/A"):
            readFunction(ser)
        
        while(float(displayVoltage.get()[:-2]) < float(chargeVoltage.get())):
            readFunction(ser)
        
        ## when capacitor bank is charged, stop charging
        ser.write("C0".encode())
        while(chargeStatus.get() != "0"):
        	readFunction(ser)
        
        ## when charging has stopped, set enable to high
        ser.write("E1".encode())
        while(enableStatus.get() != "1"):
            readFunction(ser)
        
        ## when enable is set to high, start discharge
        ser.write("D1".encode())
        while(dischargeStatus.get() != "1"):
            readFunction(ser)
    
        ## start capacitor level request stream
        ser.write("L1".encode())
        while(levelStreamStatus.get() != "1"):
              readFunction(ser)
              
        ## waiting for discharge to complete
        while(float(displayVoltage.get()[:-2]) > float(minimumRechargeVoltage.get())):
            readFunction(ser)
                
        ## stop the capacitor voltage stream once the generator has discharged sufficiently
        ser.write("L0".encode())
        while(levelStreamStatus.get() != "0"):
            readFunction(ser)
    
        ## once discharge is complete, stop discharging
        ser.write("D0".encode())
        while(dischargeStatus.get() != "0"):
            readFunction(ser)
            
        ## once discharge is set to zero, set enable to low
        ser.write("E0".encode())
        while(enableStatus.get() != "0"):
            readFunction(ser)
        
        dischargeStatus.set("-1")
        chargeStatus.set("-1")
        enableStatus.set("-1")
        levelStreamStatus.set("-1")
        displayVoltage.set("N/A")
        displayCount.set(int(displayCount.get())+1)
    
    if(int(displayCount.get()) == 1):
        messagebox.showinfo("Impulses Finished!",displayCount.get()+" impulse has been executed.")   
        displayCount.set("0")
    else:
        messagebox.showinfo("Impulses Finished!",displayCount.get()+" impulses have been executed.") 
        displayCount.set("0")
        
    btnStart1.set("Start")
    btnSet.config(state=NORMAL)
    


    
"""    
def appendCSV(reading):   
    year = datetime.datetime.now().strftime( "%Y" )
    month = datetime.datetime.now().strftime( "%m" )
    day = datetime.datetime.now().strftime( "%d" )
    hour = datetime.datetime.now().strftime( "%H" )
    minute = datetime.datetime.now().strftime( "%M" )
    second = datetime.datetime.now().strftime( "%S" )
    
    if(reading != "" and reading != "======="):
        displayVal.set(str(reading)+" g")
        fields = [year, month, day, hour, minute, second, reading]
        with open (r'Test '+str(date)+'.csv', 'a',newline='') as f:
            writer = csv.writer(f)
            writer.writerow(fields)
"""

#First Row        
labelPort1 = Label(frame1,text="COM Port:")
Port1 = Entry(frame1, textvariable=port, width = 10, justify = RIGHT)
btnDetect1 = Button(frame1,text="Detect COM",command = portScan, width = 10)
#Second Row
btnConnect1 = Button(frame1,textvariable=btnCon1,command = Connect, width = 10)
labelBaud1 = Label(frame1,text="Baud Rate:")
Baud1 = Entry(frame1, textvariable=baud_rate, width = 10, justify = RIGHT)
#Third Row
labelImpulseNumbers = Label(frame1,text="Number of Impulses:")
ImpulseNumber = Entry(frame1, textvariable=impulsesRequested, width = 10, justify = RIGHT)
btnSet = Button(frame1,textvariable=setVariablesString,command=lambda: setVariables(), width = 10, state = DISABLED)
#Fourth Row
labelChargeVoltage = Label(frame1,text="Charge Voltage (V):", justify = LEFT)
chargeVoltageNum = Entry(frame1, textvariable=chargeVoltage, width = 10, justify = RIGHT)
btnStart = Button(frame1,textvariable=btnStart1,command=lambda: startImpulses(serialPort), width = 10, state = DISABLED)
#Fifth Row
labelVoltage = Label(frame1,text="Capacitor 1 Voltage (V):", justify = LEFT)
labelVoltageNum = Label(frame1,textvariable=displayVoltage, width = 10, justify = LEFT)
#Sixth Row
labelCount = Label(frame1,text="Impulse Count:", justify = LEFT)
labelCountNum = Label(frame1,textvariable=displayCount, width = 10, justify = LEFT)
#Seventh Row
labelNumber = Label(frame1,text="Impulses Requested:", justify = LEFT)
labelNumberNum = Label(frame1,textvariable=impulsesRequested, width = 10, justify = LEFT)
#Eight Row
labelChargeVoltageSet = Label(frame1,text="Charge Voltage Setpoint (V):", justify = LEFT)
labelChargeVoltageSetNum = Label(frame1,textvariable=chargeVoltage, width = 10, justify = LEFT)

#First Row
labelPort1.grid(row = 0, sticky= "W", padx=0 ,column = 0)
Port1.grid(row = 0, sticky= "W", padx=0 ,column = 1)
btnDetect1.grid(row = 0, column = 2, pady = 5, padx = 5)
#Second Row
labelBaud1.grid(row = 1, sticky= "W", padx=0 ,column = 0)
Baud1.grid(row = 1, sticky= "W", padx=0 ,column = 1)
btnConnect1.grid(row = 1, column = 2, pady = 5, padx = 5)
#Third Row
labelImpulseNumbers.grid(row = 2, sticky= "W", padx=0 ,column = 0)
ImpulseNumber.grid(row = 2, sticky= "W", padx=0 ,column = 1)
btnSet.grid(row = 2, column = 2, pady = 5, padx = 5)
#Fourth Row
labelChargeVoltage.grid(row = 3, sticky= "W", padx=0 ,column = 0)
chargeVoltageNum.grid(row = 3, column = 1, pady = 5)
btnStart.grid(row = 3, column = 2, pady = 5, padx = 5)
#Fifth Row
labelVoltage.grid(row = 4, column = 0, pady = 5, columnspan = 2)
labelVoltageNum.grid(row = 4, column = 2, pady = 5)
#Sixth Row
labelCount.grid(row = 5, column = 0, pady = 5, columnspan = 2)
labelCountNum.grid(row = 5, column = 2, pady = 5)
#Seventh Row
labelNumber.grid(row = 6, column = 0, pady = 5, columnspan = 2)
labelNumberNum.grid(row = 6, column = 2, pady = 5, padx = 5)
#Eighth Row
labelChargeVoltageSet.grid(row = 7, column = 0, pady = 5, columnspan = 2)
labelChargeVoltageSetNum.grid(row = 7, column = 2, pady = 5, padx = 5)





frame1.pack() 


try:
    root.mainloop()
except Exception:
    print(Exception)
    